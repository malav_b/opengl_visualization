###OPENGL - VISUALIZATION CODE

######### Importing packages #############

import time
import pygame
from pygame.locals import *
import struct
import os
import sys
import pandas as pd
import math
import numpy as np

try:
    from OpenGL.GL import *
    from OpenGL.GLUT import *
    from OpenGL.GLU import *
except ImportError:
    print('The GLCUBE example requires PyOpenGL')
    raise SystemExit


########## Reading the files ###############

read_file_imu = pd.read_csv('/home/malav/Downloads/Wave_Test_IV_corrected2/Wave Test IV/data-qual-test-4/imu.csv')
#read_file_imu = pd.read_csv('/home/malav/Downloads/Wave Test IV/Wave Test IV/data-qual-test-4/imu.csv')

a_x_values = []
a_y_values = []
a_z_values = []

for i in range(0,100):
    a_x_values.append(read_file_imu['accel_x'][i])
    a_y_values.append(read_file_imu['accel_y'][i])
    a_z_values.append(read_file_imu['accel_z'][i]-1)

    #a_x_values.append(read_file_imu['fusion_qpose_x'][i])
    #a_y_values.append(read_file_imu['fusion_qpose_y'][i])
    #a_z_values.append(read_file_imu['fusion_qpose_z'][i]-1)
    # a_q_values1.append(read_file_imu['fusion_qpose_scalar'][i])

    a = a_x_values[i]
    b = a_y_values[i]
    c = a_z_values[i]


######### Making the shapes and defing its params ################

CUBE_POINTS = (
    (0.5, -0.5, -0.5), (0.5, 0.5, -0.5),
    (-0.5, 0.5, -0.5), (-0.5, -0.5, -0.5),
    (0.5, -0.5, 0.5), (0.5, 0.5, 0.5),
    (-0.5, -0.5, 0.5), (-0.5, 0.5, 0.5)
)

x1= 5

CUBE_POINTS1 = (
    (x1+0.5, x1-0.5, x1-0.5), (x1+0.5, x1+0.5, x1-0.5),
    (x1-0.5, x1+0.5, x1-0.5), (x1-0.5, x1-0.5, x1-0.5),
    (x1+0.5, x1-0.5, x1+0.5), (x1+0.5, x1+0.5, x1+0.5),
    (x1-0.5, x1-0.5, x1+0.5), (x1-0.5, x1+0.5, x1+0.5)
)

# x2 = 2
#
# arrow_r = (
#     (0, -0.1, 0.1), (0, -0.1, -0.1),
#     (x2, -0.1, 0.1), (x2, -0.1, -0.1),
#     (0, 0.1, 0.1), (0, 0.1, -0.1),
#     (x2, 0.1, 0.1), (x2, 0.1, -0.1)
# )
#
# x3 = 2
# arrow_g = (
#     (-0.1, 0, 0.1), (-0.1, 0, -0.1),
#     (-0.1, x3, 0.1), (-0.1,x3, -0.1),
#     (0.1, 0, 0.1), (0.1, 0, -0.1),
#     (0.1, x3, 0.1), (0.1, x3, -0.1)
# )
#
# x4 = 4
# arrow_b = (
#     (-0.1,  0.1 , 0), (-0.1, -0.1, 0),
#     (-0.1, 0.1, x4), (-0.1, -0.1, x4),
#     (0.1,  0.1, 0), (0.1,  -0.1, 0),
#     (0.1, 0.1, x4), (0.1,  -0.1, x4)
# )
#


############### COLORS #################
# colors are 0-1 floating values

CUBE_COLORS = (
    (1, 0, 0), (1, 1, 0), (0, 1, 0), (0, 0, 0),
    (1, 0, 1), (1, 1, 1), (0, 0, 1), (0, 1, 1)
)

CUBE_COLORS1 = (
    (1, 0, 0), (1, 1, 0), (0, 1, 0), (0, 0, 0),
    (1, 0, 1), (1, 1, 1), (0, 0, 1), (0, 1, 1)
)

arrow_r_colors = (
    (1, 0, 0), (1, 0, 0), (1, 0, 0), (1, 0, 0),
    (1, 0, 0), (1, 0, 0), (1, 0, 0), (1, 0, 0)
)

arrow_g_colors = (
    (0, 1, 0), (0, 1, 0), (0, 1, 0), (0, 1, 0),
    (0, 1, 0), (0, 1, 0), (0, 1, 0), (0, 1, 0)
)

arrow_b_colors = (
    (0, 0, 1), (0, 0, 1), (0, 0, 1), (0, 0, 1),
    (0, 0, 1), (0, 0, 1), (0, 0, 1), (0, 0, 1)
)

arrow_r_colors2 = arrow_r_colors
arrow_g_colors2 = arrow_g_colors
arrow_b_colors2 = arrow_b_colors
arrow_scale_1_colors = arrow_r_colors
arrow_scale_2_colors = CUBE_COLORS

########### VERTICES ##############

CUBE_QUAD_VERTS = (
    (0, 1, 2, 3), (3, 2, 7, 6), (6, 7, 5, 4),
    (4, 5, 1, 0), (1, 5, 7, 2), (4, 0, 3, 6)
)
CUBE_QUAD_VERTS1 = CUBE_QUAD_VERTS
arrow_r_verts = CUBE_QUAD_VERTS
arrow_g_verts = CUBE_QUAD_VERTS
arrow_b_verts = CUBE_QUAD_VERTS
arrow_r_verts2 = CUBE_QUAD_VERTS
arrow_g_verts2 = CUBE_QUAD_VERTS
arrow_b_verts2 = CUBE_QUAD_VERTS
arrow_scale_1_verts = CUBE_QUAD_VERTS
arrow_scale_2_verts = CUBE_QUAD_VERTS

############# EDGES ################

CUBE_EDGES = (
    (0, 1), (0, 3), (0, 4), (2, 1), (2, 3), (2, 7),
    (6, 3), (6, 4), (6, 7), (5, 1), (5, 4), (5, 7),
)
CUBE_EDGES1 = CUBE_EDGES
arrow_r_edges = CUBE_EDGES
arrow_g_edges = CUBE_EDGES
arrow_b_edges = CUBE_EDGES
arrow_r_edges2 = CUBE_EDGES
arrow_g_edges2 = CUBE_EDGES
arrow_b_edges2 = CUBE_EDGES
arrow_scale_1_edges = CUBE_EDGES
arrow_scale_2_edges = CUBE_EDGES

############## RAW PITCH YAW ################

angles_pry = np.array([15.0000,15.000000,15.00000])

def raw_pitch_yaw(accelX,accelY,accelZ):
    pitch = float(180 * math.atan2(accelX, math.sqrt(accelY * accelY + accelZ * accelZ)) / math.pi)
    roll = float(180 * math.atan2(accelY, math.sqrt(accelX * accelX + accelZ * accelZ)) / math.pi)
    yaw = float(180 * math.atan(accelZ / math.sqrt(accelX * accelX + accelZ * accelZ)) / math.pi)
    angles_pry[0] = float(pitch)
    angles_pry[1] = float(roll)
    angles_pry[2] = float(yaw)
    return angles_pry


############# DRAW TEXT ##################

def drawText(position, textString):
    font = pygame.font.SysFont("Courier", 18, True)
    textSurface = font.render(textString, True, (255, 255, 255, 255), (0, 0, 0, 255))
    textData = pygame.image.tostring(textSurface, "RGBA", True)
    glRasterPos3d(*position)
    glDrawPixels(textSurface.get_width(), textSurface.get_height(), GL_RGBA, GL_UNSIGNED_BYTE, textData)


############# CUBE DRAW #################

def drawcube(a_x,a_y,a_z,a1,b1,c1):
    "draw the cube"
    if a_x >= 100:
        a_x  = 99
    if a_y >= 100:
        a_y = 99
    if a_z >= 100:
        a_z = 99


    x2 = 2 #+ 10*a_x_values[a_x]
    arrow_r = (
        (0, -0.1, 0.1), (0, -0.1, -0.1),
        (x2, -0.1, 0.1), (x2, -0.1, -0.1),
        (0, 0.1, 0.1), (0, 0.1, -0.1),
        (x2, 0.1, 0.1), (x2, 0.1, -0.1)
    )


    x3 = 2 #+ a_y
    arrow_g = (
        (-0.1, 0, 0.1), (-0.1, 0, -0.1),
        (-0.1, x3, 0.1), (-0.1, x3, -0.1),
        (0.1, 0, 0.1), (0.1, 0, -0.1),
        (0.1, x3, 0.1), (0.1, x3, -0.1)
    )

    x4 = 2 #+ a_z
    arrow_b = (
        (-0.1, 0.1, 0), (-0.1, -0.1, 0),
        (-0.1, 0.1, x4), (-0.1, -0.1, x4),
        (0.1, 0.1, 0), (0.1, -0.1, 0),
        (0.1, 0.1, x4), (0.1, -0.1, x4)
    )

    x5 = -2 + 10*a_x_values[a_x]
    arrow_r2 = (
        (5, 4.9, 5.1), (5, 4.9, 4.9),
        (x5+5, 4.9, 5.1), (x5+5, 4.9, 4.9),
        (5, 5.1, 5.1), (5, 5.1, 4.9),
        (x5+5, 5.1, 5.1), (x5+5, 5.1, 4.9)
    )

    x6 = 2 + 10*a_y_values[a_x]
    arrow_g2 = (
        (4.9, 5, 5.1), (4.9, 5, 4.9),
        (4.9, x6+5, 5.1), (4.9, x6+5, 4.9),
        (5.1, 5, 5.1), (5.1, 5, 4.9),
        (5.1, x6+5, 5.1), (5.1, x6+5, 4.9)
    )

    x7 = -2 + 10*a_z_values[a_x]
    arrow_b2 = (
        (4.9, 5.1, 5), (4.9, 4.9, 5),
        (4.9, 5.1, x7+5), (4.9, 4.9, x7+5),
        (5.1, 5.1, 5), (5.1, 4.9, 5),
        (5.1, 5.1, x7+5), (5.1, 4.9, x7+5)
    )

    x2 = -13
    arrow_scale_1 = (
        (-8, 0.8, 1.2), (-8, 0.8, 0.8),
        (x2, 0.8, 1.2), (x2, 0.8, 0.8),
        (-8, 1.2, 1.2), (-8, 1.2, 0.8),
        (x2, 1.2, 1.2), (x2, 1.2, 0.8)
    )

    # x2 = -10
    # arrow_scale_2 = (
    #     (-8, 0.9, 1.1), (-8, 0.9, 0.9),
    #     (x2, 0.9, 1.1), (x2, 0.9, 0.9),
    #     (-8, 1.1, 1.1), (-8, 1.1, 0.9),
    #     (x2, 1.1, 1.1), (x2, 1.1, 0.9)
    # )

    allpoints = list(zip(CUBE_POINTS, CUBE_COLORS))
    allpoints1 = list(zip(CUBE_POINTS1, CUBE_COLORS1))
    allpoints2 = list(zip(arrow_r, arrow_r_colors))
    allpoints3 = list(zip(arrow_g, arrow_g_colors))
    allpoints4 = list(zip(arrow_b, arrow_b_colors))
    allpoints5 = list(zip(arrow_r2, arrow_r_colors2))
    allpoints6 = list(zip(arrow_g2, arrow_g_colors2))
    allpoints7 = list(zip(arrow_b2, arrow_b_colors2))
    allpoints8 = list(zip(arrow_scale_1, arrow_scale_1_colors)) ##LOAD CELL DATA NOW
    #allpoints9 = list(zip(arrow_scale_2, arrow_scale_2_colors))

    text = "pitch_s: " + str("{0:.5f}".format(a1)) + ", roll_s: " + str(
        "{0:.5f}".format(b1))  # ", yaw: " + str("{0:.2f}".format(az))

    yaw_mode = True

    if yaw_mode:
        osd_line = text + ", yaw_s: " + str("{0:.5f}".format(c1))
    else:
        osd_line = text

    drawText((-5, -7, -3), osd_line)

    text_imu = "pitch_imu: " + str("{0:.5f}".format(a1)) + ", roll_imu: " + str(
        "{0:.5f}".format(b1))

    osd_line_imu = text_imu + ", yaw_imu: " + str("{0:.5f}".format(c1))
    drawText((-5, 8, 2), osd_line_imu)

    text1 = "IMU"
    text2 = "SCALE"
    drawText((0, -2, -2), text2)
    drawText((4, 5, 2), text1)

    text3 = "measured_W : "
    text4 = "actual_W : "
    text5 = "Load_cell"
    drawText((-13, 3, 2), text4)
    drawText((-13, 4, 2), text3)
    drawText((-11.5, -0.5, 2), text5)

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix()
    glRotatef(a1, 1.0, 0.0, 0.0)
    glRotatef(-1*b1, 0.0, 0.0, 1.0)
    glRotatef(c1, 0.0, 1, 0.0)
    glBegin(GL_QUADS)
    for face in CUBE_QUAD_VERTS:
        for vert in face:
            pos, color = allpoints[vert]
            glColor3fv(color)
            glVertex3fv(pos)

    glEnd()
    glPopMatrix()

    # glMatrixMode(GL_MODELVIEW);
    # glLoadIdentity();
    glPushMatrix()

    # xf = cx + (px - cx) * cos(theta)) - (py - cy) * sin(theta));
    # yf = cy + (px - cx) * sin(theta)) + (py - cy) * cos(theta));
    glTranslatef(5,5,5)
    glRotatef(a1, 1.0, 0.0, 0.0)
    glRotatef(-1*b1, 0.0, 0.0, 1.0)
    glRotatef(c1, 0.0, 1, 0.0)
    glTranslatef(-5,-5,-5)
    glBegin(GL_QUADS)
    for face1 in CUBE_QUAD_VERTS1:
        for vert1 in face1:
            pos1, color1 = allpoints1[vert1]
            glColor3fv(color1)
            glVertex3fv(pos1)

    glEnd()
    glPopMatrix()

    glBegin(GL_QUADS)
    for face2 in arrow_r_verts:
        for vert2 in face2:
            pos2, color2 = allpoints2[vert2]
            glColor3fv(color2)
            glVertex3fv(pos2)
    glEnd()

    glBegin(GL_QUADS)
    for face3 in arrow_g_verts:
        for vert3 in face3:
            pos3, color3 = allpoints3[vert3]
            glColor3fv(color3)
            glVertex3fv(pos3)
    glEnd()

    glBegin(GL_QUADS)
    for face4 in arrow_b_verts:
        for vert4 in face4:
            pos4, color4 = allpoints4[vert4]
            glColor3fv(color4)
            glVertex3fv(pos4)
    glEnd()

    glBegin(GL_QUADS)
    for face5 in arrow_r_verts2:
        for vert5 in face5:
            pos5, color5 = allpoints5[vert5]
            glColor3fv(color5)
            glVertex3fv(pos5)
    glEnd()

    glBegin(GL_QUADS)
    for face6 in arrow_g_verts2:
        for vert6 in face6:
            pos6, color6 = allpoints6[vert6]
            glColor3fv(color6)
            glVertex3fv(pos6)
    glEnd()

    glBegin(GL_QUADS)
    for face7 in arrow_b_verts2:
        for vert7 in face7:
            pos7, color7 = allpoints7[vert7]
            glColor3fv(color7)
            glVertex3fv(pos7)
    glEnd()

    glBegin(GL_QUADS)
    for face8 in arrow_scale_1_verts:
        for vert8 in face8:
            pos8, color8 = allpoints8[vert8]
            glColor3fv(color8)
            glVertex3fv(pos8)
    glEnd()
    #
    # glBegin(GL_QUADS)
    # for face9 in arrow_scale_2_verts:
    #     for vert9 in face9:
    #         pos9, color9 = allpoints9[vert9]
    #         glColor3fv(color9)
    #         glVertex3fv(pos9)
    # glEnd()

    glColor3f(1.0, 1.0, 1.0)
    glBegin(GL_LINES)
    # for line in CUBE_EDGES:
    #   for vert in line:
    #     pos, color = allpoints[vert]
    #     glVertex3fv(pos)
    for x in range(-50, 50):
        glColor3f(x * .03 % 1.0, x * .04 % 1.0, x * .05 % 1.0)
        glVertex3fv(( 0, x, -100))
        glVertex3fv(( 0, x ,100))
        glVertex3fv((-100, x, 0))
        glVertex3fv((100, x, 0))

    glEnd()

#
# ############### CAD MODEL ##################
# # class for a 3d point
# class createpoint:
#     def __init__(self, p, c=(1, 0, 0)):
#         self.point_size = 0.5
#         self.color = c
#         self.x = p[0]
#         self.y = p[1]
#         self.z = p[2]
#
#     def glvertex(self):
#         glVertex3f(self.x, self.y, self.z)
#
#
# #class for a 3d face on a model
# class createtriangle:
#     points = None
#     normal = None
#
#     def __init__(self, p1, p2, p3, n=None):
#         # 3 points of the triangle
#         self.points = createpoint(p1), createpoint(p2), createpoint(p3)
#
#         # triangles normal
#         self.normal = createpoint(self.calculate_normal(self.points[0], self.points[1], self.points[2]))  # (0,1,0)#
#
#     # calculate vector / edge
#     def calculate_vector(self, p1, p2):
#         return -p1.x + p2.x, -p1.y + p2.y, -p1.z + p2.z
#
#     def calculate_normal(self, p1, p2, p3):
#         a = self.calculate_vector(p3, p2)
#         b = self.calculate_vector(p3, p1)
#         # calculate the cross product returns a vector
#         return self.cross_product(a, b)
#
#     def cross_product(self, p1, p2):
#         return (p1[1] * p2[2] - p2[1] * p1[2]), (p1[2] * p2[0]) - (p2[2] * p1[0]), (p1[0] * p2[1]) - (p2[0] * p1[1])
#
# class loader:
#     model = []
#
#     # return the faces of the triangles
#     def get_triangles(self):
#         if self.model:
#             for face in self.model:
#                 yield face
#
#     # draw the models faces
#     def draw(self):
#         glBegin(GL_TRIANGLES)
#         for tri in self.get_triangles():
#             glNormal3f(tri.normal.x, tri.normal.y, tri.normal.z)
#             glVertex3f(tri.points[0].x, tri.points[0].y, tri.points[0].z)
#             glVertex3f(tri.points[1].x, tri.points[1].y, tri.points[1].z)
#             glVertex3f(tri.points[2].x, tri.points[2].y, tri.points[2].z)
#         glEnd()
#
#     # load stl file detects if the file is a text file or binary file
#     def load_stl(self, filename):
#         # read start of file to determine if its a binay stl file or a ascii stl file
#         fp = open(filename, 'rb')
#         h = fp.read(80)
#         type = h[0:5]
#         fp.close()
#
#         if type == 'solid':
#             print
#             "reading text file" + str(filename)
#             self.load_text_stl(filename)
#         else:
#             print
#             "reading binary stl file " + str(filename, )
#             self.load_binary_stl(filename)
#
#     # read text stl match keywords to grab the points to build the model
#     def load_text_stl(self, filename):
#         fp = open(filename, 'r')
#
#         for line in fp.readlines():
#             words = line.split()
#             if len(words) > 0:
#                 if words[0] == 'solid':
#                     self.name = words[1]
#
#                 if words[0] == 'facet':
#                     center = [0.0, 0.0, 0.0]
#                     triangle = []
#                     normal = (eval(words[2]), eval(words[3]), eval(words[4]))
#
#                 if words[0] == 'vertex':
#                     triangle.append((eval(words[1]), eval(words[2]), eval(words[3])))
#
#                 if words[0] == 'endloop':
#                     # make sure we got the correct number of values before storing
#                     if len(triangle) == 3:
#                         self.model.append(createtriangle(triangle[0], triangle[1], triangle[2], normal))
#         fp.close()
#
#     # load binary stl file check wikipedia for the binary layout of the file
#     # we use the struct library to read in and convert binary data into a format we can use
#     def load_binary_stl(self, filename):
#         fp = open(filename, 'rb')
#         h = fp.read(80)
#
#         l = struct.unpack('I', fp.read(4))[0]
#         count = 0
#         while True:
#             try:
#                 p = fp.read(12)
#                 if len(p) == 12:
#                     n = struct.unpack('f', p[0:4])[0], struct.unpack('f', p[4:8])[0], struct.unpack('f', p[8:12])[0]
#
#                 p = fp.read(12)
#                 if len(p) == 12:
#                     p1 = struct.unpack('f', p[0:4])[0], struct.unpack('f', p[4:8])[0], struct.unpack('f', p[8:12])[0]
#
#                 p = fp.read(12)
#                 if len(p) == 12:
#                     p2 = struct.unpack('f', p[0:4])[0], struct.unpack('f', p[4:8])[0], struct.unpack('f', p[8:12])[0]
#
#                 p = fp.read(12)
#                 if len(p) == 12:
#                     p3 = struct.unpack('f', p[0:4])[0], struct.unpack('f', p[4:8])[0], struct.unpack('f', p[8:12])[0]
#
#                 new_tri = (n, p1, p2, p3)
#
#                 if len(new_tri) == 4:
#                     tri = createtriangle(p1, p2, p3, n)
#                     self.model.append(tri)
#                 count += 1
#                 fp.read(2)
#
#                 if len(p) == 0:
#                     break
#             except EOFError:
#                 break
#         fp.close()
#
# class draw_scene:
#     def __init__(self, style=1):
#         # create a model instance and
#         self.model1 = loader()
#         # self.model1.load_stl(os.path.abspath('')+'/text.stl')
#         self.model1.load_stl(os.path.abspath('') + '/arrow_design.stl')
#         self.init_shading()
#
#     # solid model with a light / shading
#     def init_shading(self):
#         glShadeModel(GL_SMOOTH)
#         glClearColor(0.0, 0.0, 0.0, 0.0)
#         glClearDepth(1)
#         glEnable(GL_DEPTH_TEST)
#         glShadeModel(GL_SMOOTH)
#         glDepthFunc(GL_LEQUAL)
#         glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)
#
#         glEnable(GL_COLOR_MATERIAL)
#         glEnable(GL_LIGHTING)
#         glEnable(GL_LIGHT0)
#         glLight(GL_LIGHT0, GL_POSITION, (0, 1, 1, 0))
#         glMatrixMode(GL_MODELVIEW)
#
#     def draw(self):
#         glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
#         glLoadIdentity()
#
#         glTranslatef(0.0, 0.0, 0.0)
#         glScale(0.20, 0.20, 0.20)
#         self.model1.draw()


####### FORCES DEFINITION ######

m_k =1 #mass of ship
acc = 9.8 #force from ship to scale
gravity = 9.8 #gravity
m_b = 1 #mass of bin
m_t = 7.5 #mass of top pan
m_l = 0.5 #mass of load cell
z_t = 1 # z-axis of scale
c1 = -0.5/(gravity)
c2 = m_t + (0.5*m_l)

f_k = m_k*(acc + gravity)
f_t = -(m_b + m_t)*acc
f_s = -(m_b + m_t + m_l)*acc
w = ((-c1*(2*m_b + 2*m_t + m_l))*(np.dot(acc, z_t))) + m_t + (0.5*m_l)
w_new = (((w - m_t - 0.5*m_l) * np.dot(-gravity, z_t))/np.dot(acc, z_t)) + m_t + 0.5*m_l

############# ADDING KEYBOARD COMMANDS ###########

def keyboard(key, x, y):
    ch = key.decode("utf-8")
    print(type(key), key, type(ch), ch)
    # Allow to quit by pressing 'Esc' or 'q'
    if ch == chr(27):
        #glRotatef(1, 0, 1, 0)
        sys.exit()
    if ch == 'q':
        print('Good bye!')
    #sys.exit()

def main():
    "run the demo"
    # initialize pygame and setup an opengl display
    pygame.init()
    glutInit()
    pygame.display.set_mode((1024, 768), OPENGL | DOUBLEBUF)
    glEnable(GL_DEPTH_TEST)  # use our zbuffer

    # setup the camera
    qobj = gluNewQuadric();
    gluQuadricNormals(qobj, GLU_SMOOTH);
    glMatrixMode(GL_PROJECTION)
    gluPerspective(60.0, 1024 / 768.0, 0.1, 100.0)  # setup lens
    #glOrtho(100.0, 100.0, 0, 100.0, 1.0, 10.0);
    glTranslatef(0.0, 0.0, -20.0)  # move back
    glRotatef(0, 1, 0, 0)  # orbit higher
    glPushMatrix();
    #glRotatef(90., 1., 0., 0.);
    gluCylinder(qobj, 10, 10, 20, 360, 120);
    glPopMatrix();
    glutKeyboardFunc(keyboard)
    nt = int(time.time() * 1000)
    global CUBE_POINTS1
    i1 = 0
    #scene = draw_scene()    #### ADDING CAD IN SCENE

    for i in range(100000000):
        nt += 20

        #glTranslatef(0.0, 0.0, -.1)

        # check for quit'n events
        event = pygame.event.poll()
        if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
            break

        # clear screen and move camera
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        #scene.draw()    ##### ADDING CAD IN SCENE
        # orbit camera around by 1 degree
        #glRotatef(1, 0, 1, 0)

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                glTranslate(-0.2,0,0)
            if event.key == pygame.K_LEFT:
                glTranslate(0.2, 0, 0)
            if event.key == pygame.K_UP:
                glTranslate(0, 0.2, 0)
            if event.key == pygame.K_DOWN:
                glTranslate(0, -0.2, 0)
            if event.key == pygame.K_0:
                 glRotatef(1, 0, 1, 0)
            if event.key == pygame.K_9:
                glRotatef(1, 0, -1, 0)
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 3:
                glTranslate(0, 0, 1.0)
            if event.button == 1:
                glTranslate(0, 0, -1.0)

        i1 = i
        if i1>=100:
            i1=99
        raw_pitch_yaw(a_x_values[i1], a_y_values[i1], a_z_values[i1])
        ax = float(angles_pry[0])
        ay = float(angles_pry[1])
        az = float(angles_pry[2])

        #glRotatef(1, 3, 1, 1)

        drawcube(i,i,i,ax,ay,az)
        pygame.display.flip()
        ct = int(time.time() * 1000)
        pygame.time.wait(max(1, nt - ct))
        if i<50:
            time.sleep(0.1)
        # if i % 50 == 0:
        #     print(nt - ct)


if __name__ == '__main__':
    main()